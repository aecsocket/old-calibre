package me.aecsocket.calibre;

import me.aecsocket.calibre.data.FireDelay;
import me.aecsocket.calibre.item.Item;
import me.aecsocket.calibre.item.ItemManager;
import me.aecsocket.calibre.item.gun.Gun;
import me.aecsocket.calibre.ray.RayBullet;
import me.aecsocket.calibre.ray.RayBulletManager;
import org.bukkit.entity.Entity;

class Loop {
    static void run() {
        for (Entity ent : FireDelay.getDelays().keySet()) {
            int delay = FireDelay.getDelay(ent);
            if (delay > 0)
                FireDelay.setDelay(ent, delay - 1);
        }

        for (RayBulletManager manager : Calibre.getInstance().getRayManagers()) {
            if (manager.getBullets().size() > 0) {
                RayBullet[] bullets = manager.getBullets().toArray(new RayBullet[0]);
                for (RayBullet bullet : bullets)
                    if (!bullet.tick()) manager.removeBullet(bullet);
            }
        }

        for (ItemManager manager : Calibre.getInstance().getItemManagers()) {
            for (Item item : manager.getItems().values()) {
                if (item instanceof Gun) {
                    Gun gun = (Gun)item;
                    if (gun.getRecoil() > 0.001)
                        gun.setRecoil(gun.getRecoil() * gun.getFinalStats().getAccuracy().getRecoilRecover());
                    else
                        gun.setRecoil(0.001);
                    if (gun.getDelay() > 0)
                        gun.setDelay(gun.getDelay() - 1);
                }
            }
        }
    }
}
