package me.aecsocket.calibre.stats;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;

public class DropoffInfo implements ConfigurationSerializable {
    private double start;
    private Vector drop;
    private double velocity;
    private double damage;

    public DropoffInfo(double start, Vector drop, double velocity, double damage) {
        this.start = start;
        this.drop = drop;
        this.velocity = velocity;
        this.damage = damage;
    }

    public double getStart()                    { return start; }
    public void setStart(double start)          { this.start = start; }
    public Vector getDrop()                     { return drop.clone(); }
    public void setDrop(Vector drop)            { this.drop = drop; }
    public double getVelocity()                 { return velocity; }
    public void setVelocity(double velocity)    { this.velocity = velocity; }
    public double getDamage()                   { return damage; }
    public void setDamage(double damage)        { this.damage = damage; }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<>();

        result.put("start", start);
        result.put("drop", drop);
        result.put("velocity", velocity);
        result.put("damage", damage);

        return result;
    }
    public static DropoffInfo deserialize(Map<String, Object> args) {
        double start = 0.0d;
        Vector drop = new Vector(0, 0, 0);
        double velocity = 0.0d;
        double damage = 0.0d;

        if(args.containsKey("start"))
            start = (double)args.get("start");
        if(args.containsKey("drop"))
            drop = (Vector)args.get("drop");
        if(args.containsKey("velocity"))
            velocity = (double)args.get("velocity");
        if(args.containsKey("damage"))
            damage = (double)args.get("damage");

        return new DropoffInfo(start, drop, velocity, damage);
    }

    @Override
    public DropoffInfo clone() {
        try {
            return (DropoffInfo)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error("Did Java break again?");
        }
    }
}
