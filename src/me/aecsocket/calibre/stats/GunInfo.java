package me.aecsocket.calibre.stats;

import me.aecsocket.calibre.munition.Calibre;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GunInfo implements ConfigurationSerializable, Cloneable {
    private int delay;
    private BallisticInfo ballistics;
    private AccuracyInfo accuracy;
    private boolean singleFire;
    private boolean chambers;
    private int pellets;
    private int fireTimes;
    private Calibre[] calibres;

    public GunInfo(int delay, BallisticInfo ballistics, AccuracyInfo accuracy, boolean singleFire, boolean chambers, int pellets, Calibre[] calibres) {
        this.delay = delay;
        this.ballistics = ballistics;
        this.accuracy = accuracy;
        this.singleFire = singleFire;
        this.chambers = chambers;
        this.pellets = pellets;
        this.calibres = calibres;
    }

    public int getDelay()                               { return delay; }
    public void setDelay(int delay)                     { this.delay = delay; }
    public BallisticInfo getBallistics()                { return ballistics; }
    public void setBallistics(BallisticInfo ballistics) { this.ballistics = ballistics; }
    public AccuracyInfo getAccuracy()                   { return accuracy; }
    public void setAccuracy(AccuracyInfo accuracy)      { this.accuracy = accuracy; }
    public boolean isSingleFire()                       { return singleFire; }
    public void setSingleFire(boolean singleFire)       { this.singleFire = singleFire; }
    public boolean chambers()                           { return chambers; }
    public void setChambers(boolean chambers)           { this.chambers = chambers; }
    public int getPellets()                             { return pellets; }
    public void setPellets(int pellets)                 { this.pellets = pellets; }
    public Calibre[] getCalibres()                      { return calibres; }
    public void setCalibres(Calibre[] calibres)         { this.calibres = calibres; }

    @Override
    public GunInfo clone() {
        try {
            return (GunInfo)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error("Did Java break again?");
        }
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<>();

        result.put("delay", delay);
        result.put("ballistics", ballistics);
        result.put("accuracy", accuracy);
        result.put("single-fire", singleFire);
        result.put("chambers", chambers);
        result.put("pellets", pellets);
        result.put("calibres", calibres);

        return result;
    }
    public static GunInfo deserialize(Map<String, Object> args) {
        int delay = 0;
        BallisticInfo ballistics = new BallisticInfo(0.0d, 0.0d, 0.0f, 0.0f, 0.0f,
                new DropoffInfo(0.0d, new Vector(0, 0, 0), 0.0d, 0.0d),
                1.0d);
        AccuracyInfo accuracy = new AccuracyInfo(0.0d, 0.0d, 0.0f);
        boolean singleFire = false;
        boolean chambers = true;
        int pellets = 1;
        Calibre[] calibres;

        if (args.containsKey("delay"))
            delay = (int)args.get("delay");
        if (args.containsKey("ballistics"))
            ballistics = (BallisticInfo)args.get("ballistics");
        if (args.containsKey("accuracy"))
            accuracy = (AccuracyInfo)args.get("accuracy");
        if (args.containsKey("single-fire"))
            singleFire = (boolean)args.get("single-fire");
        if (args.containsKey("chambers"))
            chambers = (boolean)args.get("chambers");
        if (args.containsKey("pellets"))
            pellets = (int)args.get("pellets");
        calibres = ((List<Calibre>)args.get("calibres")).toArray(new Calibre[0]);

        return new GunInfo(delay, ballistics, accuracy, singleFire, chambers, pellets, calibres);
    }

    public GunInfo modify(InfoModifier[] stats) {
        if (stats != null && stats.length > 0) {
            for (InfoModifier mod : stats) {
                Statistic stat = mod.getStat();
                Operation op = mod.getOperation();
                Object obj = mod.getValue();

                switch (stat) {
                    case DELAY:
                        switch (op) {
                            case SET:
                                this.delay = (int)obj;
                                break;
                            case ADD:
                                this.delay += (int)obj;
                                break;
                            case MULTIPLY:
                                this.delay *= (double)obj;
                                break;
                        }
                        break;
                    case BALLISTICS_DAMAGE:
                        switch (op) {
                            case SET:
                                this.ballistics.setDamage((double)obj);
                                break;
                            case ADD:
                                this.ballistics.setDamage(this.ballistics.getDamage() + (double)obj);
                                break;
                            case MULTIPLY:
                                this.ballistics.setDamage(this.ballistics.getDamage() * (double)obj);
                                break;
                        }
                        break;
                    case BALLISTICS_VELOCITY:
                        switch (op) {
                            case SET:
                                this.ballistics.setVelocity((double)obj);
                                break;
                            case ADD:
                                this.ballistics.setVelocity(this.ballistics.getVelocity() + (double)obj);
                                break;
                            case MULTIPLY:
                                this.ballistics.setVelocity(this.ballistics.getVelocity() * (double)obj);
                                break;
                        }
                        break;
                    case BALLISTICS_ENV_PENETRATION:
                        switch (op) {
                            case SET:
                                this.ballistics.setEnvPenetration((float)obj);
                                break;
                            case ADD:
                                this.ballistics.setEnvPenetration(this.ballistics.getEnvPenetration() + (float)obj);
                                break;
                            case MULTIPLY:
                                this.ballistics.setEnvPenetration(this.ballistics.getEnvPenetration() * (float)obj);
                                break;
                        }
                        break;
                    case BALLISTICS_ENT_PENETRATION:
                        switch (op) {
                            case SET:
                                this.ballistics.setEntPenetration((float)obj);
                                break;
                            case ADD:
                                this.ballistics.setEntPenetration(this.ballistics.getEntPenetration() + (float)obj);
                                break;
                            case MULTIPLY:
                                this.ballistics.setEntPenetration(this.ballistics.getEntPenetration() * (float)obj);
                                break;
                        }
                        break;
                    case BALLISTICS_ARMOR_PENETRATION:
                        switch (op) {
                            case SET:
                                this.ballistics.setArmorPenetration((float)obj);
                                break;
                            case ADD:
                                this.ballistics.setArmorPenetration(this.ballistics.getArmorPenetration() + (float)obj);
                                break;
                            case MULTIPLY:
                                this.ballistics.setArmorPenetration(this.ballistics.getArmorPenetration() * (float)obj);
                                break;
                        }
                        break;
                    case BALLISTICS_DROPOFF_START:
                        switch (op) {
                            case SET:
                                this.ballistics.getDropoff().setStart((double)obj);
                                break;
                            case ADD:
                                this.ballistics.getDropoff().setStart(this.ballistics.getDropoff().getStart() + (double)obj);
                                break;
                            case MULTIPLY:
                                this.ballistics.getDropoff().setStart(this.ballistics.getDropoff().getStart() * (double)obj);
                                break;
                        }
                        break;
                    case BALLISTICS_DROPOFF_DROP:
                        switch (op) {
                            case SET:
                                this.ballistics.getDropoff().setDrop((Vector)obj);
                                break;
                            case ADD:
                                this.ballistics.getDropoff().setDrop(this.ballistics.getDropoff().getDrop().add((Vector)obj));
                                break;
                            case MULTIPLY:
                                this.ballistics.getDropoff().setDrop(this.ballistics.getDropoff().getDrop().multiply((Vector)obj));
                                break;
                        }
                        break;
                    case BALLISTICS_DROPOFF_DAMAGE:
                        switch (op) {
                            case SET:
                                this.ballistics.getDropoff().setDamage((double)obj);
                                break;
                            case ADD:
                                this.ballistics.getDropoff().setDamage(this.ballistics.getDropoff().getDamage() + (double)obj);
                                break;
                            case MULTIPLY:
                                this.ballistics.getDropoff().setDamage(this.ballistics.getDropoff().getDamage() * (double)obj);
                                break;
                        }
                        break;
                    case BALLISTICS_DROPOFF_VELOCITY:
                        switch (op) {
                            case SET:
                                this.ballistics.getDropoff().setVelocity((double)obj);
                                break;
                            case ADD:
                                this.ballistics.getDropoff().setVelocity(this.ballistics.getDropoff().getVelocity() + (double)obj);
                                break;
                            case MULTIPLY:
                                this.ballistics.getDropoff().setVelocity(this.ballistics.getDropoff().getVelocity() * (double)obj);
                                break;
                        }
                        break;
                    case BALLISTICS_WIND_MULTIPLIER:
                        switch (op) {
                            case SET:
                                this.ballistics.setWindMultiplier((double)obj);
                                break;
                            case ADD:
                                this.ballistics.setWindMultiplier(this.ballistics.getWindMultiplier() + (double)obj);
                                break;
                            case MULTIPLY:
                                this.ballistics.setWindMultiplier(this.ballistics.getWindMultiplier() * (double)obj);
                                break;
                        }
                        break;
                    case ACCURACY_INACCURACY:
                        switch (op) {
                            case SET:
                                this.accuracy.setInaccuracy((double)obj);
                                break;
                            case ADD:
                                this.accuracy.setInaccuracy(this.accuracy.getInaccuracy() + (double)obj);
                                break;
                            case MULTIPLY:
                                this.accuracy.setInaccuracy(this.accuracy.getInaccuracy() * (double)obj);
                                break;
                        }
                        break;
                    case ACCURACY_RECOIL:
                        switch (op) {
                            case SET:
                                this.accuracy.setRecoil((double)obj);
                                break;
                            case ADD:
                                this.accuracy.setRecoil(this.accuracy.getRecoil() + (double)obj);
                                break;
                            case MULTIPLY:
                                this.accuracy.setRecoil(this.accuracy.getRecoil() * (double)obj);
                                break;
                        }
                        break;
                    case ACCURACY_RECOIL_RECOVER:
                        switch (op) {
                            case SET:
                                this.accuracy.setRecoilRecover((float)obj);
                                break;
                            case ADD:
                                this.accuracy.setRecoilRecover(this.accuracy.getRecoilRecover() + (float)obj);
                                break;
                            case MULTIPLY:
                                this.accuracy.setRecoilRecover(this.accuracy.getRecoilRecover() * (float)obj);
                                break;
                        }
                        break;
                    case SINGLE_FIRE:
                        switch (op) {
                            case SET:
                                this.singleFire = (boolean)obj;
                                break;
                            case NONE:
                                break;
                            default:
                                throw new IllegalArgumentException("Cannot add or multiply boolean");
                        }
                        break;
                    case CHAMBERS:
                        switch (op) {
                            case SET:
                                this.chambers = (boolean)obj;
                                break;
                            case NONE:
                                break;
                            default:
                                throw new IllegalArgumentException("Cannot add or multiply boolean");
                        }
                        break;
                    case PELLETS:
                        switch (op) {
                            case SET:
                                this.pellets = (int)obj;
                                break;
                            case ADD:
                                this.pellets += (int)obj;
                                break;
                            case MULTIPLY:
                                this.pellets *= (double)obj;
                                break;
                        }
                        break;
                    case CALIBRES:
                        Calibre[] casted = (Calibre[])obj;
                        switch (op) {
                            case SET:
                                this.calibres = casted;
                                break;
                            case ADD:
                                Calibre[] old = calibres;
                                calibres = new Calibre[calibres.length + casted.length];
                                System.arraycopy(old, 0, calibres, 0, old.length);
                                System.arraycopy(casted, 0, calibres, calibres.length, casted.length);
                                break;
                            case NONE:
                                break;
                            default:
                                throw new IllegalArgumentException("Cannot multiply array");
                        }
                }
            }
        }
        return this;
    }
}
