package me.aecsocket.calibre.stats;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

public class AccuracyInfo implements ConfigurationSerializable {
    private double inaccuracy;
    private double recoil;
    private float recoilRecover;

    public AccuracyInfo(double inaccuracy, double recoil, float recoilRecover) {
        this.inaccuracy = inaccuracy;
        this.recoil = recoil;
        this.recoilRecover = recoilRecover;
    }

    public double getInaccuracy()                       { return inaccuracy; }
    public void setInaccuracy(double inaccuracy)        { this.inaccuracy = inaccuracy; }
    public double getRecoil()                           { return recoil; }
    public void setRecoil(double recoil)                { this.recoil = recoil; }
    public float getRecoilRecover()                    { return recoilRecover; }
    public void setRecoilRecover(float recoilRecover)   { this.recoilRecover = recoilRecover; }

    @Override
    public AccuracyInfo clone() {
        try {
            return (AccuracyInfo)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error("Did Java break again?");
        }
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<>();

        result.put("inaccuracy", inaccuracy);
        result.put("recoil", recoil);
        result.put("recoil-recover", recoilRecover);

        return result;
    }
    public static AccuracyInfo deserialize(Map<String, Object> args) {
        double inaccuracy = 0.0d;
        double recoil = 0.0d;
        float recoilRecover = 0.0f;

        if (args.containsKey("inaccuracy"))
            inaccuracy = (double)args.get("inaccuracy");
        if (args.containsKey("recoil"))
            recoil = (double)args.get("recoil");
        if (args.containsKey("recoil-recover"))
            recoilRecover = (float)(double)args.get("recoil-recover");

        return new AccuracyInfo(inaccuracy, recoil, recoilRecover);
    }
}
