package me.aecsocket.calibre.stats;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;

public class BallisticInfo implements ConfigurationSerializable, Cloneable {
    private double damage;
    private double velocity;
    private float envPenetration;
    private float entPenetration;
    private float armorPenetration;
    private DropoffInfo dropoff;
    private double windMultiplier;

    public BallisticInfo(double damage, double velocity, float envPenetration, float entPenetration, float armorPenetration, DropoffInfo dropoff, double windMultiplier) {
        this.damage = damage;
        this.velocity = velocity;
        this.envPenetration = envPenetration;
        this.entPenetration = entPenetration;
        this.armorPenetration = armorPenetration;
        this.dropoff = dropoff;
        this.windMultiplier = windMultiplier;
        check();
    }

    private void check() {
        if (!(envPenetration >= 0.0f && envPenetration <= 1.0f))
            throw new IllegalArgumentException("Environment penetration must be between 0.0 and 1.0 (" + envPenetration + ")");
        if (!(entPenetration >= 0.0f && entPenetration <= 1.0f))
            throw new IllegalArgumentException("Entity penetration must be between 0.0 and 1.0 (" + entPenetration + ")");
        if (!(armorPenetration >= 0.0f && armorPenetration <= 1.0f))
            throw new IllegalArgumentException("Armor penetration must be between 0.0 and 1.0 (" + armorPenetration + ")");
    }

    public double getDamage()                               { return damage; }
    public void setDamage(double damage)                    { this.damage = damage; }
    public double getVelocity()                             { return velocity; }
    public void setVelocity(double velocity)                { this.velocity = velocity; }
    public float getEnvPenetration()                        { return envPenetration; }
    public void setEnvPenetration(float envPenetration)     { this.envPenetration = envPenetration; check(); }
    public float getEntPenetration()                        { return entPenetration; }
    public void setEntPenetration(float entPenetration)     { this.entPenetration = entPenetration; check(); }
    public float getArmorPenetration()                      { return armorPenetration; }
    public void setArmorPenetration(float armorPenetration) { this.armorPenetration = armorPenetration; }
    public DropoffInfo getDropoff()                         { return dropoff; }
    public void setDropoff(DropoffInfo dropoff)             { this.dropoff = dropoff; }
    public double getWindMultiplier()                       { return windMultiplier; }
    public void setWindMultiplier(double windMultiplier)    { this.windMultiplier = windMultiplier; }

    @Override
    public BallisticInfo clone() {
        try {
            return (BallisticInfo)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error("Did Java break again?");
        }
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<>();

        result.put("damage", damage);
        result.put("velocity", velocity);
        result.put("environment-penetration", envPenetration);
        result.put("entity-penetration", entPenetration);
        result.put("armor-penetration", armorPenetration);
        result.put("dropoff", dropoff);
        result.put("wind-multiplier", windMultiplier);

        return result;
    }
    public static BallisticInfo deserialize(Map<String, Object> args) {
        double damage = 0.0d;
        double velocity = 0.0d;
        float envPenetration = 0.0f;
        float entPenetration = 0.0f;
        float armorPenetration = 0.0f;
        DropoffInfo dropoff = new DropoffInfo(0.0d, new Vector(0, 0, 0), 0.0d, 0.0d);
        double windMultiplier = 1.0d;

        if (args.containsKey("damage"))
            damage = (double)args.get("damage");
        if (args.containsKey("velocity"))
            velocity = (double)args.get("velocity");
        if (args.containsKey("environment-penetration"))
            envPenetration = (float)(double)args.get("environment-penetration");
        if (args.containsKey("entity-penetration"))
            entPenetration = (float)(double)args.get("entity-penetration");
        if (args.containsKey("armor-penetration"))
            armorPenetration = (float)(double)args.get("armor-penetration");
        if (args.containsKey("dropoff"))
            dropoff = (DropoffInfo)args.get("dropoff");
        if (args.containsKey("wind-multiplier"))
            windMultiplier = (double)args.get("wind-multiplier");

        return new BallisticInfo(damage, velocity, envPenetration, entPenetration, armorPenetration, dropoff, windMultiplier);
    }
}
