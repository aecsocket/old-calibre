package me.aecsocket.calibre.stats;

public enum Operation {
    SET,
    ADD,
    MULTIPLY,
    NONE
}
