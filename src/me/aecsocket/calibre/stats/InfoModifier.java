package me.aecsocket.calibre.stats;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

public class InfoModifier implements ConfigurationSerializable {
    private Statistic stat;
    private Operation op;
    private Object value;

    public InfoModifier(Statistic stat, Operation op, Object value) {
        this.stat = stat;
        this.op = op;
        this.value = value;
    }

    public Statistic getStat()      { return stat; }
    public Operation getOperation() { return op; }
    public Object getValue()        { return value; }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<>();

        result.put("stat", stat.name());
        result.put("operation", op.name());
        result.put("value", value);

        return result;
    }
    public static InfoModifier deserialize(Map<String, Object> args) {
        Statistic stat;
        Operation op;
        Object value;

        stat = Statistic.valueOf((String)args.get("stat"));
        op = Operation.valueOf((String)args.get("operation"));
        value = args.get("value");

        return new InfoModifier(stat, op, value);
    }
}
