package me.aecsocket.calibre;

import org.bukkit.World;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;

public class WindManager {
    private Map<World, Vector> wind;

    public WindManager() {
        this.wind = new HashMap<>();
    }

    public Map<World, Vector> getWinds()            { return new HashMap<>(wind); }
    public Vector getWind(World world)              { return wind.getOrDefault(world, new Vector(0, 0, 0)); }
    public boolean hasWind(World world)             { return wind.containsKey(wind); }
    public void setWind(World world, Vector wind)   { this.wind.put(world, wind); }
    public void removeWind(World world)             { wind.remove(world); }
}
