package me.aecsocket.calibre.item;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public interface Item extends ConfigurationSerializable {
    UUID getUniqueId();
    ItemStack getBaseItem();
    void setBaseItem(ItemStack baseItem);

    ItemStack toItem();
}
