package me.aecsocket.calibre.item.gun;

import me.aecsocket.calibre.stats.GunInfo;
import me.aecsocket.calibre.stats.InfoModifier;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FireMode implements ConfigurationSerializable, Cloneable {
    private InfoModifier[] mods;

    public FireMode(InfoModifier[] mods) {
        this.mods = mods;
    }

    public InfoModifier[] getMods()             { return mods; }
    public void setMods(InfoModifier[] mods)    { this.mods = mods; }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<>();

        result.put("mods", mods);

        return result;
    }
    public static FireMode deserialize(Map<String, Object> args) {
        InfoModifier[] mods = null;

        if (args.containsKey("mods"))
            mods = ((List<InfoModifier>)args.get("mods")).toArray(new InfoModifier[0]);

        return new FireMode(mods);
    }

    @Override
    public FireMode clone() {
        try {
            return (FireMode)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error("Did Java break again?");
        }
    }
}
