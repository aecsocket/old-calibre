package me.aecsocket.calibre.item.gun;

import me.aecsocket.calibre.WindManager;
import me.aecsocket.calibre.data.FireDelay;
import me.aecsocket.calibre.hook.Hook;
import me.aecsocket.calibre.ray.RayBullet;
import me.aecsocket.calibre.item.Item;
import me.aecsocket.calibre.item.ItemManager;
import me.aecsocket.calibre.munition.Ammunition;
import me.aecsocket.calibre.munition.Bullet;
import me.aecsocket.calibre.ray.RayBulletManager;
import me.aecsocket.calibre.stats.GunInfo;
import me.aecsocket.calibre.stats.InfoModifier;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.*;

public class Gun implements Item {
    final private UUID uuid;
    private ItemStack baseItem;
    private ItemStack sightItem;
    private boolean inSights;
    private InfoModifier[] sightMods;
    private GunInfo stats;
    private Bullet chamber;
    private Ammunition ammo;
    private FireMode fireMode;
    private FireMode[] fireModes;
    private Hook[] hooks;
    private int delay;
    private double recoil;

    public Gun(UUID uuid, ItemStack baseItem, ItemStack sightItem, boolean inSights, InfoModifier[] sightMods, GunInfo stats, Bullet chamber, Ammunition ammunition, FireMode fireMode, FireMode[] fireModes, Hook[] hooks) {
        this.uuid = uuid;
        this.baseItem = baseItem;
        this.sightItem = sightItem;
        this.inSights = inSights;
        this.sightMods = sightMods;
        this.stats = stats;
        this.chamber = chamber;
        this.ammo = ammunition;
        this.fireMode = fireMode;
        this.fireModes = fireModes;
        this.hooks = hooks;
        this.delay = 0;
        this.recoil = recoil;
    }

    public Gun(ItemStack baseItem, ItemStack sightItem, boolean inSights, InfoModifier[] sightMods, GunInfo stats, Bullet chamber, Ammunition ammunition, FireMode fireMode, FireMode[] fireModes, Hook[] hooks) {
        this(UUID.randomUUID(), baseItem, sightItem, inSights, sightMods, stats, chamber, ammunition, fireMode, fireModes, hooks);
    }

    @Override
    public UUID getUniqueId()                       { return uuid; }
    @Override
    public ItemStack getBaseItem()                  { return baseItem.clone(); }
    @Override
    public void setBaseItem(ItemStack baseItem)     { this.baseItem = baseItem; }
    public ItemStack getSightItem()                 { return sightItem.clone(); }
    public void setSightItem(ItemStack sightItem)   { this.sightItem = sightItem; }
    public boolean inSights()                       { return inSights; }
    public void setInSights(boolean inSights)       { this.inSights = inSights; }
    public InfoModifier[] getSightMods()            { return sightMods; }
    public void setSightMods(InfoModifier[] mods)   { this.sightMods = mods;}
    public GunInfo getStats()                       { return stats; }
    public void setStats(GunInfo stats)             { this.stats = stats; }
    public Bullet getChamber()                      { return chamber; }
    public void setChamber(Bullet chamber)          { this.chamber = chamber; }
    public Ammunition getAmmo()                     { return ammo; }
    public void setAmmo(Ammunition ammo)            { this.ammo = ammo; }
    public FireMode getFireMode()                   { return fireMode; }
    public void setFireMode(FireMode fireMode)      { this.fireMode = fireMode; }
    public FireMode[] getFireModes()                { return fireModes; }
    public void setFireModes(FireMode[] fireModes)  { this.fireModes = fireModes; }
    public Hook[] getHooks()                        { return hooks; }
    public void setHooks(Hook[] hooks)              { this.hooks = hooks; }
    public int getDelay()                           { return delay; }
    public void setDelay(int delay)                 { this.delay = delay; }
    public double getRecoil()                       { return recoil; }
    public void setRecoil(double recoil)            { this.recoil = recoil; }
    public GunInfo getFinalStats() {
        GunInfo finalStats = stats.clone();
        if (chamber != null) {
            if (chamber.getCalibre().getMods() != null)
                finalStats.modify(chamber.getCalibre().getMods());
            if (chamber.getType().getMods() != null)
                finalStats.modify(chamber.getType().getMods());
        }
        if (inSights && sightMods != null)
            finalStats.modify(sightMods);
        if (fireMode.getMods() != null)
            finalStats.modify(fireMode.getMods());
        if (hooks != null && hooks.length > 0)
            for (Hook hook : hooks)
                if (hook.getMod() != null && hook.getMod().getMods() != null)
                    finalStats.modify(hook.getMod().getMods());
        return finalStats;
    }

    public Gun clone(UUID newUuid) {
        return new Gun(
                newUuid, baseItem.clone(), (sightItem != null ? sightItem.clone() : null), inSights, sightMods, stats.clone(), (chamber != null ? chamber.clone() : null), (ammo != null ? ammo.clone() : null), fireMode, fireModes, hooks);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<>();

        result.put("uuid", uuid.toString());
        result.put("base-item", baseItem);
        result.put("sight-item", sightItem);
        result.put("sight-mods", sightMods);
        result.put("stats", stats);
        result.put("chamber", chamber);
        result.put("ammo", ammo);
        result.put("fire-mode", fireMode);
        result.put("fire-modes", fireModes);
        result.put("hooks", hooks);

        return result;
    }
    public static Gun deserialize(Map<String, Object> args) {
        UUID uuid;
        ItemStack baseItem;
        ItemStack sightItem = null;
        InfoModifier[] sightMods = null;
        GunInfo stats;
        Bullet chamber = null;
        Ammunition ammo = null;
        FireMode fireMode;
        FireMode[] fireModes;
        Hook[] hooks = null;

        uuid = UUID.fromString((String)args.get("uuid"));
        baseItem = (ItemStack)args.get("base-item");
        if (args.containsKey("sight-item"))
            sightItem = (ItemStack)args.get("sight-item");
        if (args.containsKey("sight-mods"))
            sightMods = (args.get("sight-mods") == null ? null : ((List<InfoModifier>)args.get("sight-mods")).toArray(new InfoModifier[0]));
        stats = (GunInfo)args.get("stats");
        if (args.containsKey("chamber"))
            chamber = (Bullet)args.get("chamber");
        if (args.containsKey("ammo"))
            ammo = (Ammunition)args.get("ammo");
        fireMode = (FireMode)args.get("fire-mode");
        fireModes = ((List<FireMode>)args.get("fire-modes")).toArray(new FireMode[0]);
        if (args.containsKey("hooks"))
            hooks = (args.get("hooks") == null ? null : ((List<Hook>)args.get("hooks")).toArray(new Hook[0]));

        return new Gun(uuid, baseItem, sightItem, false, sightMods, stats, chamber, ammo, fireMode, fireModes, hooks);
    }

    public boolean fire(Entity owner, Location location, Vector direction, RayBulletManager rayManager, WindManager windManager) {
        Gun gun = this;
        GunInfo finalStats = getFinalStats();
        if (finalStats.isSingleFire() && owner != null && FireDelay.getDelay(owner) > 0) {
            FireDelay.setDelay(owner, 5);
        }
        if (delay <= 0) {
            if (owner != null)
                FireDelay.setDelay(owner, 5);

            if ((finalStats.chambers() && chamber == null) || (!finalStats.chambers() && (ammo == null || !ammo.hasBullets()))) {
                if (finalStats.chambers())
                    chamber();
            } else {
                Bullet fired = (finalStats.chambers() ? chamber : ammo.getNextBullet());
                if (finalStats.chambers())
                    chamber();
                else
                    ammo.removeNextBullet();

                for (int i = 0; i < finalStats.getPellets(); i++) {
                    Random rnd = new Random();
                    double inaccuracy = finalStats.getAccuracy().getInaccuracy() + recoil;
                    direction.add(new Vector(
                            ((rnd.nextDouble() - 0.5) * inaccuracy),
                            ((rnd.nextDouble() - 0.5) * inaccuracy),
                            ((rnd.nextDouble() - 0.5) * inaccuracy)
                    ));
                    RayBullet bullet = new RayBullet(gun, owner, finalStats.getBallistics(), fired.getType(), location, direction, windManager);
                    recoil += finalStats.getAccuracy().getRecoil();
                    if (finalStats.getBallistics().getVelocity() > 0.0d)
                        rayManager.addBullet(bullet);
                    else
                        while (bullet.step());
                }
                return true;
            }
        }
        return false;
    }

    public boolean fire(Location location, Vector direction, RayBulletManager rayManager, WindManager windManager) {
        return fire(null, location, direction, rayManager, windManager);
    }

    public void chamber() {
        if (ammo != null && ammo.hasBullets()) {
            chamber = ammo.getNextBullet();
            ammo.removeNextBullet();
        } else
            chamber = null;
    }

    public void cycleFireMode() {
        int i = 0;
        for (int j = 0; j < fireModes.length; j++) {
            if (fireModes[j] == fireMode) {
                i = j;
                break;
            }
        }
        ++i;
        i = (i >= fireModes.length ? 0 : i);
        fireMode = fireModes[i];
    }

    @Override
    public ItemStack toItem() {
        ItemStack item = (inSights ? sightItem : baseItem).clone();
        ItemMeta meta = item.getItemMeta();

        List<String> lore = (meta.getLore() == null ? new ArrayList<>() : meta.getLore());
        lore.add("");
        lore.add(ChatColor.DARK_GRAY + uuid.toString());
        meta.setLore(lore);

        item.setItemMeta(meta);
        return item;
    }
    public static Gun toGun(ItemStack item, ItemManager manager) throws IllegalArgumentException {
        if (item != null) {
            ItemMeta meta = item.getItemMeta();
            if (meta != null && meta.getLore() != null && meta.getLore().size() >= 2) {
                List<String> lore = meta.getLore();
                UUID uuid = UUID.fromString(ChatColor.stripColor(lore.get(lore.size() - 1)));
                if (manager.hasItem(uuid) && manager.getItem(uuid) instanceof Gun)
                    return (Gun) manager.getItem(uuid);
            }
        }
        return null;
    }
    public static boolean isGun(ItemStack item, ItemManager manager) {
        return toGun(item, manager) != null;
    }
}
