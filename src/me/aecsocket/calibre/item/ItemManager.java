package me.aecsocket.calibre.item;

import me.aecsocket.calibre.exception.DuplicateKeyException;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ItemManager {
    final static public String PREFIX = "calibre.guns";
    private Map<UUID, Item> items;
    final private FileConfiguration config;

    public ItemManager(FileConfiguration config) {
        items = new HashMap<>();
        this.config = config;
    }

    public Map<UUID, Item> getItems()       { return items; }
    public FileConfiguration getConfig()    { return config; }
    public Item getItem(UUID uuid)          { return items.getOrDefault(uuid, null); }
    public boolean hasItem(UUID uuid)       { return items.containsKey(uuid); }
    public void addItem(Item item) throws DuplicateKeyException {
        if (items.containsKey(item.getUniqueId()))
            throw new DuplicateKeyException();
        else
            items.put(item.getUniqueId(), item);
    }
    public void unloadItem(UUID uuid)       { items.remove(uuid); }

    public void removeItem(UUID uuid, File file) {
        items.put(uuid, null);
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        items.remove(uuid);
    }

    public void save(File file) {
        for (Item item : items.values()) {
            config.set(PREFIX + "." + item.getUniqueId(), item);
        }
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public Item load(UUID uuid) {
        if (config.contains(PREFIX + "." + uuid)) {
            Object obj = config.get(PREFIX + "." + uuid);
            if (obj instanceof Item)
                return (Item)obj;
        }
        return null;
    }

    public void loadAll() {
        items.clear();
        if (config.isConfigurationSection(PREFIX)) {
            ConfigurationSection section = config.getConfigurationSection(PREFIX);
            for (String key : section.getValues(false).keySet()) {
                UUID uuid;
                try {
                    uuid = UUID.fromString(key);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    continue;
                }
                Item item = load(uuid);
                if (item != null)
                    items.put(item.getUniqueId(), item);
            }
        }
    }
}
