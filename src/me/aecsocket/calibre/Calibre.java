package me.aecsocket.calibre;

import me.aecsocket.calibre.hook.Hook;
import me.aecsocket.calibre.item.ItemManager;
import me.aecsocket.calibre.item.gun.FireMode;
import me.aecsocket.calibre.item.gun.Gun;
import me.aecsocket.calibre.munition.Ammunition;
import me.aecsocket.calibre.munition.Bullet;
import me.aecsocket.calibre.munition.Regular;
import me.aecsocket.calibre.ray.RayBulletManager;
import me.aecsocket.calibre.stats.AccuracyInfo;
import me.aecsocket.calibre.stats.BallisticInfo;
import me.aecsocket.calibre.stats.DropoffInfo;
import me.aecsocket.calibre.stats.GunInfo;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class Calibre extends JavaPlugin {
    private static Calibre instance;
    private List<RayBulletManager> rayManagers = new ArrayList<>();
    private List<ItemManager> itemManagers = new ArrayList<>();

    public static Calibre getInstance() { return instance; }

    @Override
    public void onEnable() {
        instance = this;

        ConfigurationSerialization.registerClass(Gun.class);
        ConfigurationSerialization.registerClass(FireMode.class);

        ConfigurationSerialization.registerClass(BallisticInfo.class);
        ConfigurationSerialization.registerClass(DropoffInfo.class);
        ConfigurationSerialization.registerClass(AccuracyInfo.class);
        ConfigurationSerialization.registerClass(GunInfo.class);

        ConfigurationSerialization.registerClass(Ammunition.class);
        ConfigurationSerialization.registerClass(Bullet.class);
        ConfigurationSerialization.registerClass(Regular.class);

        ConfigurationSerialization.registerClass(Hook.class);

        getServer().getScheduler().scheduleSyncRepeatingTask(this, Loop::run, 1L, 1L);
    }

    public List<RayBulletManager> getRayManagers()                      { return new ArrayList<>(rayManagers); }
    public boolean rayManagerRegistered(RayBulletManager rayManager)    { return rayManagers.contains(rayManager); }
    public void registerRayManager(RayBulletManager rayManager)         { rayManagers.add(rayManager); }
    public void unregisterRayManager(RayBulletManager rayManager)       { rayManagers.remove(rayManager); }

    public List<ItemManager> getItemManagers()                          { return new ArrayList<>(itemManagers); }
    public boolean itemManagerRegistered(ItemManager itemManager)       { return itemManagers.contains(itemManager); }
    public void registerItemManager(ItemManager itemManager)            { itemManagers.add(itemManager); }
    public void unregisterItemManager(ItemManager itemManager)          { itemManagers.remove(itemManager); }
}
