package me.aecsocket.calibre.hook;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

public class Hook implements ConfigurationSerializable {
    private Mod mod;

    public Hook(Mod mod) {
        this.mod = mod;
    }

    public Mod getMod()         { return mod; }
    public void setMod(Mod mod) { this.mod = mod; }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<>();

        result.put("mod", mod);

        return result;
    }
    public static Hook deserialize(Map<String, Object> args) {
        Mod mod = null;

        if (args.containsKey("mod"))
            mod = (args.get("mod") == null ? null : (Mod)args.get("mod"));

        return new Hook(mod);
    }
}
