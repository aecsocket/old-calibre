package me.aecsocket.calibre.hook;

import me.aecsocket.calibre.stats.InfoModifier;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

public interface Mod extends ConfigurationSerializable {
    InfoModifier[] getMods();
    void setMods(InfoModifier[] mods);
}
