package me.aecsocket.calibre.ray;

import java.util.ArrayList;
import java.util.List;

public class RayBulletManager {
    private List<RayBullet> rayBullets;

    public RayBulletManager() {
        this.rayBullets = new ArrayList<>();
    }

    public List<RayBullet> getBullets()         { return rayBullets; }
    public void addBullet(RayBullet bullet)     { rayBullets.add(bullet); }
    public void removeBullet(RayBullet bullet)  { rayBullets.remove(bullet); }
}
