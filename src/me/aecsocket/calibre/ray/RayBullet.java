package me.aecsocket.calibre.ray;

import me.aecsocket.calibre.Penetration;
import me.aecsocket.calibre.WindManager;
import me.aecsocket.calibre.item.gun.Gun;
import me.aecsocket.calibre.munition.BulletType;
import me.aecsocket.calibre.stats.BallisticInfo;
import net.minecraft.server.v1_13_R2.AxisAlignedBB;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class RayBullet implements Cloneable {
    final private Gun shooter;
    final private Entity owner;
    final private BallisticInfo initialBallistics;
    private BallisticInfo ballistics;
    final private Location origin;
    private Location location;
    private Vector direction;
    private Vector drop;
    private Vector windMovement;
    private boolean inLiquid;
    private List<Entity> hitEntities;
    private BulletType type;
    private WindManager windManager;

    public RayBullet(Gun shooter, Entity owner, BallisticInfo ballistics, BulletType type, Location origin, Vector direction, WindManager windManager) {
        this.shooter = shooter;
        this.owner = owner;
        this.initialBallistics = ballistics.clone();
        this.ballistics = ballistics.clone();
        this.origin = origin.clone();
        this.location = origin.clone();
        this.drop = new Vector(0, 0, 0);
        this.windMovement = new Vector(0, 0, 0);
        this.inLiquid = false;
        this.hitEntities = new ArrayList<>();
        this.direction = direction.clone();
        this.type = type;
        this.windManager = windManager;
    }

    public Gun getShooter()                             { return shooter; }
    public Entity getOwner()                            { return owner; }
    public BallisticInfo getInitialBallistics()         { return initialBallistics.clone(); }
    public BallisticInfo getBallistics()                { return ballistics; }
    public void setBallistics(BallisticInfo ballistics) { this.ballistics = ballistics; }
    public Location getOrigin()                         { return origin.clone(); }
    public Location getLocation()                       { return location.clone(); }
    private void setLocation(Location location)         { this.location = location; }
    public Vector getDirection()                        { return direction.clone(); }
    public void setDirection(Vector direction)          { this.direction = direction; }
    public Vector getDrop()                             { return drop.clone(); }
    public void setDrop(Vector drop)                    { this.drop = drop; }
    public Vector getWindMovement()                     { return windMovement.clone(); }
    public void setWindMovement(Vector windMovement)    { this.windMovement = windMovement; }
    public boolean inLiquid()                           { return inLiquid; }
    public void setInLiquid(boolean inLiquid)           { this.inLiquid = inLiquid; }
    public List<Entity> getHitEntities()                { return new ArrayList<>(hitEntities); }
    public BulletType getType()                         { return type; }
    private void setType(BulletType type)               { this.type = type; }
    public WindManager getWindManager()                 { return windManager; }
    public void setWindManager(WindManager windManager) { this.windManager = windManager; }

    public boolean tick() {
        for (int i = 0; i < (int)(ballistics.getVelocity() * 10); i++) {
            if (!step())
                return false;
        }
        return true;
    }

    public boolean step() {
        location.add(direction.normalize().multiply(0.1f));
        location.add(drop);
        location.add(windMovement);
        double distance = origin.distance(location);

        Block block = location.getBlock();
        Material blockType = block.getType();
        boolean isLiquid = block.isLiquid();
        if (isLiquid && !inLiquid) {
            inLiquid = true;
            type.enterLiquid(this, block);
        }
        if (inLiquid && !isLiquid) {
            inLiquid = false;
            type.exitLiquid(this, block);
        }
        if (blockType == Material.AIR || blockType == Material.CAVE_AIR || blockType == Material.VOID_AIR ||
            blockType == Material.WATER || blockType == Material.LAVA)
            type.travel(this);
        else
            type.hitBlock(this, block);

        Collection<Entity> entities = location.getWorld().getNearbyEntities(location, 8, 8, 8);
        for (Entity ent : entities) {
            if (ent != owner && !hitEntities.contains(ent)) {
                AxisAlignedBB boundingBox = ((CraftEntity)ent).getHandle().getBoundingBox();
                if (boundingBox.c(new AxisAlignedBB(
                        location.getX() - 0.1, location.getY() - 0.1, location.getZ() - 0.1,
                        location.getX() + 0.1, location.getY() + 0.1, location.getZ() + 0.1))) {
                    type.hitEntity(this, ent);
                    hitEntities.add(ent);
                }
            }
        }

        if (distance > ballistics.getDropoff().getStart()) {
            ballistics.setVelocity(ballistics.getVelocity() + ballistics.getDropoff().getVelocity());
            ballistics.setDamage(ballistics.getDamage() + ballistics.getDropoff().getDamage());
            drop.add(ballistics.getDropoff().getDrop());
        }
        if (windManager != null)
            windMovement.add(windManager.getWind(location.getWorld()));

        return !(ballistics.getDamage() < 0.3d);
    }

    @Override
    public RayBullet clone() {
        try {
            return (RayBullet)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error("Did Java break again?");
        }
    }
}
