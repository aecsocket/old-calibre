package me.aecsocket.calibre;

import org.bukkit.Material;

public final class Penetration {
    public static double getPenetration(Material material) {
        switch (material) {
            case AIR:
            case CAVE_AIR:
            case VOID_AIR:

            case GRASS:
            case TALL_GRASS:

            case FERN:
            case LARGE_FERN:
            case DEAD_BUSH:
            case SEAGRASS:
            case TALL_SEAGRASS:
            case SEA_PICKLE:

            case SUNFLOWER:
            case LILAC:
            case ROSE_BUSH:
            case PEONY:

            case DANDELION:
            case POPPY:
            case BLUE_ORCHID:
            case ALLIUM:
            case AZURE_BLUET:
            case RED_TULIP:
            case ORANGE_TULIP:
            case WHITE_TULIP:
            case PINK_TULIP:
            case OXEYE_DAISY:
            case BROWN_MUSHROOM:
            case RED_MUSHROOM:

            case TORCH:
            case WALL_TORCH:
            case REDSTONE_TORCH:
            case REDSTONE_WALL_TORCH:
            case LEVER:
            case TRIPWIRE:
            case TRIPWIRE_HOOK:

            case STONE_BUTTON:
            case OAK_BUTTON:
            case SPRUCE_BUTTON:
            case BIRCH_BUTTON:
            case JUNGLE_BUTTON:
            case ACACIA_BUTTON:
            case DARK_OAK_BUTTON:

            case HEAVY_WEIGHTED_PRESSURE_PLATE:
            case LIGHT_WEIGHTED_PRESSURE_PLATE:
            case OAK_PRESSURE_PLATE:
            case SPRUCE_PRESSURE_PLATE:
            case BIRCH_PRESSURE_PLATE:
            case JUNGLE_PRESSURE_PLATE:
            case ACACIA_PRESSURE_PLATE:
            case DARK_OAK_PRESSURE_PLATE:

            case OAK_SAPLING:
            case SPRUCE_SAPLING:
            case BIRCH_SAPLING:
            case JUNGLE_SAPLING:
            case ACACIA_SAPLING:
            case DARK_OAK_SAPLING:

            case COBWEB:

            case WATER:
            case LAVA:
                return 1.0d;

            default:
                return 0.0d;
        }
    }
}
