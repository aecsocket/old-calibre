package me.aecsocket.calibre.munition;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

public class Bullet implements ConfigurationSerializable, Cloneable {
    private Calibre calibre;
    private BulletType type;

    public Bullet(Calibre calibre, BulletType type) {
        this.calibre = calibre;
        this.type = type;
    }

    public Calibre getCalibre()             { return calibre; }
    public void setCalibre(Calibre calibre) { this.calibre = calibre; }
    public BulletType getType()             { return type; }
    public void setType(BulletType type)    { this.type = type; }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<>();

        result.put("calibre", calibre);
        result.put("type", type);

        return result;
    }
    public static Bullet deserialize(Map<String, Object> args) {
        Calibre calibre;
        BulletType type;

        calibre = (Calibre)args.get("calibre");
        type = (BulletType)args.get("type");

        return new Bullet(calibre, type);
    }

    @Override
    public Bullet clone() {
        try {
            return (Bullet)super.clone();
        } catch (CloneNotSupportedException e) {
            throw new Error("Did Java break again?");
        }
    }
}
