package me.aecsocket.calibre.munition;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

interface ICalibre extends ConfigurationSerializable {
    boolean equals(Calibre other);
}
