package me.aecsocket.calibre.munition;

import me.aecsocket.calibre.stats.InfoModifier;

public abstract class BulletType implements IBulletType {
    public InfoModifier[] getMods()             { return null; }
    public void setMods(InfoModifier[] mods)    {}
}
