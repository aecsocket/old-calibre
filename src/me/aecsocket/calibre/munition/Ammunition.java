package me.aecsocket.calibre.munition;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ammunition implements ConfigurationSerializable, Cloneable {
    private List<Bullet> bullets;
    private int maxBullets;

    public Ammunition(List<Bullet> bullets, int maxBullets) {
        this.bullets = bullets;
        this.maxBullets = maxBullets;
        check();
    }

    public Ammunition(Bullet bullet, int bullets, int maxBullets) {
        List<Bullet> bulletList = new ArrayList<>();
        for (int i = 0; i < bullets; i++)
            bulletList.add(bullet);
        this.bullets = bulletList;
        this.maxBullets = maxBullets;
        check();
    }

    public Ammunition(Bullet bullet, int bullets) {
        this(bullet, bullets, bullets);
    }

    private void check() {
        if (bullets.size() > maxBullets)
            while (bullets.size() > maxBullets)
                bullets.remove(maxBullets);
    }

    public List<Bullet> getBullets()                { return bullets; }
    public void setBullets(List<Bullet> bullets)    { this.bullets = bullets; check(); }
    public int getMaxBullets()                      { return maxBullets; }
    public void setMaxBullets(int maxBullets)       { this.maxBullets = maxBullets; check(); }
    public int getIntBullets()                      { return bullets.size(); }
    public boolean hasBullets()                     { return bullets.size() > 0; }
    public Bullet getNextBullet()                   { return (bullets.size() > 0 ? bullets.get(bullets.size() - 1) : null); }
    public void removeNextBullet()                  { if (bullets.size() > 0) bullets.remove(bullets.size() - 1); }

    @Override
    public Ammunition clone() {
        try {
            Ammunition ammunition = (Ammunition)super.clone();
            ammunition.bullets = new ArrayList<>(bullets);
            return ammunition;
        } catch (CloneNotSupportedException e) {
            throw new Error("Did Java break again?");
        }
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> result = new HashMap<>();

        result.put("bullets", bullets);
        result.put("max-bullets", maxBullets);

        return result;
    }
    public static Ammunition deserialize(Map<String, Object> args) {
        List<Bullet> bullets;
        int maxBullets;

        bullets = (List<Bullet>)args.get("bullets");
        maxBullets = bullets.size();
        if (args.containsKey("max-bullets"))
            maxBullets = (int)args.get("max-bullets");

        return new Ammunition(bullets, maxBullets);
    }
}
