package me.aecsocket.calibre.munition;

import me.aecsocket.calibre.Penetration;
import me.aecsocket.calibre.ray.RayBullet;
import me.aecsocket.calibre.stats.BallisticInfo;
import org.bukkit.*;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityRegainHealthEvent;

import java.util.HashMap;
import java.util.Map;

public class Regular extends BulletType {
    @Override
    public Map<String, Object> serialize() {
        return new HashMap<>();
    }
    public static Regular deserialize(Map<String, Object> args) {
        return new Regular();
    }

    @Override
    public void travel(RayBullet bullet) {
        Location loc = bullet.getLocation();
        World w = loc.getWorld();
        loc.getWorld().spawnParticle(Particle.END_ROD, loc, 1, 0, 0, 0, Integer.MAX_VALUE);
        if (bullet.inLiquid()) {
            w.spawnParticle(Particle.WATER_BUBBLE, loc, 8, 0.25d, 0.25d, 0.25d, 0);
        }
    }

    @Override
    public void hitBlock(RayBullet bullet, Block block) {
        double penetration = Penetration.getPenetration(block.getType());
        BallisticInfo ballistics = bullet.getBallistics();
        ballistics.setDamage(ballistics.getDamage() * (ballistics.getEnvPenetration() * penetration));
        bullet.setBallistics(ballistics);
    }

    @Override
    public void hitEntity(RayBullet bullet, Entity entity) {
        BallisticInfo ballistics = bullet.getBallistics();
        if (entity instanceof LivingEntity) {
            Entity owner = bullet.getOwner();
            ballistics.setDamage(ballistics.getDamage() * (1 - ((((LivingEntity)entity).getAttribute(Attribute.GENERIC_ARMOR).getValue() / 20.0d)) * ballistics.getArmorPenetration()));
            if (owner != null)
                ((LivingEntity)entity).damage(ballistics.getDamage(), owner);
            else
                ((LivingEntity)entity).damage(ballistics.getDamage());
        } else
            entity.remove();
        ballistics.setDamage(ballistics.getDamage() * ballistics.getEntPenetration());
        bullet.setBallistics(ballistics);
    }

    @Override
    public void enterLiquid(RayBullet bullet, Block liquid) {
        BallisticInfo ballistics = bullet.getBallistics();
        ballistics.setDamage(ballistics.getDamage() * ballistics.getEnvPenetration());
        ballistics.setVelocity(ballistics.getVelocity() * ballistics.getEnvPenetration());
        bullet.setBallistics(ballistics);
        Location loc = bullet.getLocation();
        World w = loc.getWorld();
        switch (liquid.getType()) {
            case WATER:
                w.spawnParticle(Particle.WATER_SPLASH, loc, 16, 0.25d, 0.25d, 0.25d, 0);
                break;
            case LAVA:
                w.spawnParticle(Particle.DRIP_LAVA, loc, 16, 0.25d, 0.25d, 0.25d, 0);
                break;
        }
        w.playSound(loc, Sound.ENTITY_PLAYER_SPLASH_HIGH_SPEED, SoundCategory.PLAYERS, 1.0f, 1.0f);
    }

    @Override
    public void exitLiquid(RayBullet bullet, Block liquid) {
        Location loc = bullet.getLocation();
        World w = loc.getWorld();
        w.playSound(loc, Sound.ENTITY_PLAYER_SPLASH, SoundCategory.PLAYERS, 1.0f, 1.0f);
    }

    @Override
    public void end(RayBullet bullet) {}
}
