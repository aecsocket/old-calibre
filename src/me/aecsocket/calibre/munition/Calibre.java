package me.aecsocket.calibre.munition;

import me.aecsocket.calibre.stats.GunInfo;
import me.aecsocket.calibre.stats.InfoModifier;

public abstract class Calibre implements ICalibre {
    public InfoModifier[] getMods()             { return null; }
    public void setMods(InfoModifier[] mods)    {}
}
