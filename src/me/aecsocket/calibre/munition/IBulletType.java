package me.aecsocket.calibre.munition;

import me.aecsocket.calibre.ray.RayBullet;
import org.bukkit.block.Block;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Entity;

interface IBulletType extends ConfigurationSerializable {
    void travel(RayBullet bullet);
    void hitBlock(RayBullet bullet, Block block);
    void hitEntity(RayBullet bullet, Entity entity);
    void enterLiquid(RayBullet bullet, Block liquid);
    void exitLiquid(RayBullet bullet, Block liquid);
    void end(RayBullet bullet);
}
