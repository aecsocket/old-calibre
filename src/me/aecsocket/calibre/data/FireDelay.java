package me.aecsocket.calibre.data;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public final class FireDelay {
    private static Map<Entity, Integer> delay = new HashMap<>();

    public static Map<Entity, Integer> getDelays()          { return new HashMap<>(delay); }
    public static int getDelay(Entity entity)               { return delay.getOrDefault(entity, 0); }
    public static void setDelay(Entity entity, int delay)   { FireDelay.delay.put(entity, delay); }
    public static void removeDelay(Entity entity)           { delay.remove(entity); }
}
